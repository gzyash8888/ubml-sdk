/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.json.element;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.openatom.ubml.model.common.definition.cef.IGspCommonField;
import org.openatom.ubml.model.common.definition.cef.collection.GspAssociationKeyCollection;
import org.openatom.ubml.model.common.definition.cef.collection.GspFieldCollection;
import org.openatom.ubml.model.common.definition.cef.element.AssoModelInfo;
import org.openatom.ubml.model.common.definition.cef.element.ForeignKeyConstraintType;
import org.openatom.ubml.model.common.definition.cef.element.GspAssociation;
import org.openatom.ubml.model.common.definition.cef.element.GspDeleteRuleType;
import org.openatom.ubml.model.common.definition.cef.json.CefNames;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;

import static com.fasterxml.jackson.core.JsonToken.FIELD_NAME;

/**
 * The Json Parser Of GspAssociation
 *
 * @ClassName: GspAssociationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspAssociationDeserializer extends JsonDeserializer<GspAssociation> {
    CefFieldDeserializer filedDeserializer;

    public GspAssociationDeserializer(CefFieldDeserializer filedDeserializer) {
        this.filedDeserializer = filedDeserializer;
    }

    @Override
    public GspAssociation deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        return deserializeGspAssociation(jsonParser);
    }

    public GspAssociation deserializeGspAssociation(JsonParser jsonParser) {
        GspAssociation asso = CreateGspAssociation();
        SerializerUtils.readStartObject(jsonParser);
        while (jsonParser.getCurrentToken() == FIELD_NAME) {
            String propName = SerializerUtils.readPropertyName(jsonParser);
            readPropertyValue(asso, propName, jsonParser);
        }
        SerializerUtils.readEndObject(jsonParser);
        return asso;
    }

    private void readPropertyValue(GspAssociation asso, String propName, JsonParser jsonParser) {
        switch (propName) {
            case CefNames.ID:
                asso.setId(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.REF_MODEL_ID:
                asso.setRefModelID(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.REF_MODEL_NAME:
                asso.setRefModelName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.REF_MODEL_CODE:
                asso.setRefModelCode(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.REF_MODEL_PKG_NAME:
                asso.setRefModelPkgName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.ASSO_REF_OBJECT_ID:
                asso.setRefObjectID(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.ASSO_REF_OBJECT_CODE:
                asso.setRefObjectCode(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.ASSO_REF_OBJECT_NAME:
                asso.setRefObjectName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.KEY_COLLECTION:
                readKeyCollection(jsonParser, asso);
                break;
            case CefNames.REF_ELEMENT_COLLECTION:
                readRefElementCollection(jsonParser, asso);
                break;
            case CefNames.WHERE:
                asso.setWhere(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.ASS_SEND_MESSAGE:
                asso.setAssSendMessage(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.FOREIGN_KEY_CONSTRAINT_TYPE:
                asso.setForeignKeyConstraintType(SerializerUtils.readPropertyValue_Enum(jsonParser, ForeignKeyConstraintType.class, ForeignKeyConstraintType.values()));
                break;
            case CefNames.DELETE_RULE_TYPE:
                asso.setDeleteRuleType(SerializerUtils.readPropertyValue_Enum(jsonParser, GspDeleteRuleType.class, GspDeleteRuleType.values()));
                break;
            case CefNames.I18N_RESOURCE_INFO_PREFIX:
                asso.setI18nResourceInfoPrefix(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.ASSO_MODEL_INFO:
                asso.setAssoModelInfo(readAssoModelInfo(jsonParser));
                break;
            default:
                if (!ReadExtendAssoProperty(asso, propName, jsonParser)) {
                    throw new RuntimeException(String.format("GspAssociationDeserializer未识别的属性名：%1$s", propName));
                }
        }
    }

    private void readKeyCollection(JsonParser jsonParser, GspAssociation asso) {
        GspAssociationKeyCollection collection = new GspAssociationKeyCollection();
        GspAssoKeyDeserializer der = new GspAssoKeyDeserializer();
        SerializerUtils.readArray(jsonParser, der, collection);
        asso.setKeyCollection(collection);
    }

    private void readRefElementCollection(JsonParser jsonParser, GspAssociation asso) {
        GspFieldCollection collection = new GspFieldCollection();
        SerializerUtils.readArray(jsonParser, filedDeserializer, collection);
        handleRefElementCollection(collection, asso);
        asso.setRefElementCollection(collection);
    }

    private void handleRefElementCollection(GspFieldCollection collection, GspAssociation asso) {
        for (IGspCommonField field : collection) {
            field.setParentAssociation(asso);
            // 关联带出字段应为false
            // Bug 305714: 字段非必填，关联出的字段必填，生成表单时显示关联出的字段不应该为必填
            field.setIsRequire(false);
        }
    }

    private AssoModelInfo readAssoModelInfo(JsonParser jsonParser) {
        AssoModelInfo info = new AssoModelInfo();
        if (SerializerUtils.readNullObject(jsonParser)) {
            return info;
        }
        SerializerUtils.readStartObject(jsonParser);
        while (jsonParser.getCurrentToken() == FIELD_NAME) {
            String propName = SerializerUtils.readPropertyName(jsonParser);
            readAssoModelInfoPropValue(jsonParser, propName, info);
        }
        SerializerUtils.readEndObject(jsonParser);
        return info;
    }

    private void readAssoModelInfoPropValue(JsonParser jsonParser, String propName, AssoModelInfo info) {
        switch (propName) {
            case CefNames.MODEL_CONFIG_ID:
                info.setModelConfigId(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.MAIN_OBJ_CODE:
                info.setMainObjCode(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            default:
                throw new RuntimeException(String.format("GspAssociationDeserializer未识别的属性名：%1$s", propName));
        }
    }

    protected GspAssociation CreateGspAssociation() {
        return new GspAssociation();
    }

    protected boolean ReadExtendAssoProperty(GspAssociation asso, String propName, JsonParser jsonParser) {
        return false;
    }
}
