/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.commonmodel.json;

/**
 * The Serializer Names Of  Common Model
 *
 * @ClassName: CmObjectSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonModelNames {

    public static final String MODEL = "Model";

    public static final String EXTEND_LIST = "ExtendList";

    public static final String EXTEND_MODEL = "ExtendModel";

    public static final String MAIN_OBJECT = "MainObject";

    public static final String VARIABLES = "Variables";

    public static final String VARIABLES_JSON = "Variables_Json";

    public static final String CHILD_OBJECT_LIST = "ChildObjectList";

    public static final String ELEMENT_LIST = "ElementList";

    public static final String ELEMENT = "Element";

    public static final String CHILD_OBJECT = "ChildObject";

    public static final String BE_LABEL = "BeLabel";

    /**
     * 分级信息
     */
    public static final String HIRARCHY_INFO = "HirarchyInfo";

    /**
     * 分级字段
     */
    public static final String PATH_ELEMENT = "PathElement";

    public static final String PATH_LENGTH = "PathLength";

    public static final String PARENT_ELEMENT = "ParentElement";

    public static final String PARENT_REF_ELEMENT = "ParentRefElement";

    public static final String PATH_GENERATE_TYPE = "PathGenerateType";

    /**
     * sListCode
     */
    public static final String LAYER_ELEMENT = "LayerElement";

    /**
     * sListCode
     */
    public static final String DETAIL_ELEMENT = "IsDetailElement";

    public static final String ID = "ID";

    public static final String GENERATE_ID = "ColumnGenerateID";

    public static final String ELEMENT_ID = "ElementID";

    public static final String GENERATE_TYPE = "GenerateType";

    public static final String GERNERATE_TYPE = "GernerateType";

    public static final String CODE = "Code";

    public static final String NAME = "Name";

    public static final String IS_RECORD = "IsRecord";

    public static final String RECORD_HISTORY_TABLE = "RecordHistoryTable";

    public static final String LOG_OBJECT_ID = "LogObjectID";

    public static final String IS_VIRTUAL = "IsVirtual";

    public static final String ENTITY_TYPE = "EntityType";

    public static final String REF_OBJECT_NAME = "RefObjectName";

    public static final String OBJECT_TYPE = "ObjectType";

    public static final String LOGIC_DELETE = "LogicDelete";

    public static final String COLUMN = "COLUMN";

    public static final String GENERATETYPE = "GENERATETYPE";

    public static final String ORDERBY_CONDITION = "OrderbyCondition";

    public static final String FILTER_CONDITION = "FilterCondition";

    public static final String MODIFIED_DATE_ELEMENT_ID = "ModifiedDateElementID";


    public static final String CREATOR_ELEMENT_ID = "CreatorElementID";

    public static final String DATE_ELEMENT_ID = "CreatedDateElementID";

    public static final String MODIFIER_ELEMENT_ID = "ModifierElementID";

    public static final String RECORD_DEL_DATA = "RecordDelData";

    public static final String IS_READ_ONLY = "IsReadOnly";

    public static final String CONSTRAINTS = "Constraints";

    public static final String CONSTRAINT = "Constraint";

    public static final String CONSTRAINT_TYPE = "ConstraintType";

    public static final String CONSTRAINT_MESSAGE = "ConstraintMessage";

    public static final String CONSTRAINT_COLUMNS = "ConstraintColumns";

    public static final String CONSTRAINT_COLUMN = "ConstraintColumn";


    public static final String MDATA_TYPE = "MDataType";

    public static final String LABEL_ID = "LabelID";

    public static final String BILL_CODE = "CanBillCode";

    public static final String BILL_CODE_ID = "BillCodeID";

    public static final String BILL_CODE_NAME = "BillCodeName";

    public static final String CODE_GENERATE_TYPE = "CodeGenerateType";

    public static final String CODE_GENERATE_OCCASION = "CodeGenerateOccasion";

    public static final String DEFAULT_VALUE = "DefaultValue";

    public static final String DEFAULT_VALUE_TYPE = "DefaultValueType";

    public static final String LENGTH = "Length";

    public static final String PRECISION = "Precision";

    public static final String COLUMN_ID = "ColumnID";

    public static final String CUSTOM_EXPRESSION = "CustomExpression";

    public static final String IS_REQUIRE = "IsRequire";

    public static final String READONLY = "Readonly";

    public static final String IS_REF_ELEMENT = "IsRefElement";

    public static final String REF_ELEMENT_ID = "RefElementID";

    public static final String IS_MULTI_LANGUAGE = "IsMultiLanguage";

    public static final String CHILD_ASSOCIATIONS = "ChildAssociations";

    public static final String ASSOCIATION = "Association";

    public static final String IS_CUSTOM_ITEM = "IsCustomItem";

    public static final String REF_MODEL_ID = "RefModelID";

    public static final String REF_MODEL_CODE = "RefModelCode";

    public static final String REF_MODEL_NAME = "RefModelName";

    public static final String REF_MODEL_PKG_NAME = "RefModelPkgName";

    public static final String ASSO_REF_OBJECT_ID = "RefObjectID";

    public static final String ASSO_REF_OBJECT_CODE = "RefObjectCode";

    public static final String ASSO_REF_OBJECT_NAME = "RefObjectName";

    public static final String KEY_COLLECTION = "KeyCollection";

    public static final String ASSOCIATION_KEY = "AssociationKey";

    public static final String REF_ELEMENT_COLLECTION = "RefElementCollection";

    public static final String WHERE = "Where";

    public static final String ASS_SEND_MESSAGE = "AssSendMessage";

    public static final String FOREIGN_KEY_CONSTRAINT_TYPE = "ForeignKeyConstraintType";

    public static final String DELETE_RULE_TYPE = "DeleteRuleType";

    public static final String JOIN_MODE = "JoinMode";

    public static final String SOURCE_ELEMENT = "SourceElement";

    public static final String SOURCE_ELEMENT_DISPLAY = "SourceElementDisplay";

    public static final String TARGET_ELEMENT = "TargetElement";

    public static final String TARGET_ELEMENT_DISPLAY = "TargetElementDisplay";

    public static final String ENUM_VALUES = "EnumValues";

    public static final String ENUM_VALUE = "EnumValue";

    public static final String VALUE = "Value";

    public static final String IS_ALLOW_DERIVE = "IsAllowDerive";

    public static final String IS_ALLOW_EXTEND = "IsAllowExtend";

    public static final String DIMENSION = "Dimension";

    public static final String HIERARCHY = "Hierarchy";

    public static final String STATE_ELEMENT_ID = "StateElementID";

    public static final String BELONG_MODEL_ID = "BelongModelID";

    public static final String IS_REF = "IsRef";

    public static final String IS_FROM_ASSO_UDT = "IsFromAssoUdt";

    public static final String ENABLE_DYNAMIC_PROP = "EnableDynProp";

    public static final String REPOSITORY_COMPS = "RepositoryComps";

    // 字段集合类型
    public static final String COLLECTION_TYPE = "CollectionType";
    /**
     * 外键关联集合
     */
    public static final String FK_CONSTRAINTS = "FkConstraints";

    /**
     * 外键关联
     */
    public static final String FK_CONSTRAINT = "FkConstraint";

    //为json序列化新增
    public static final String CONTAIN_ENUM_VALUES = "ContainEnumValues";

    public static final String BILL_CODE_CONFIG = "BillCodeConfig";

    public static final String CONTAIN_ELEMENTS = "ContainElements";


    public static final String CONTAIN_CHILD_OBJECTS = "ContainChildObjects";

    public static final String PRIMAY_KEY_ID = "PrimayKeyID";

    public static final String EXTEND_NODE_LIST = "ExtendNodeList";

    public static final String EXT_PROPERTIES = "ExtProperties";

    public static final String RESOURCE = "Resource";

    public static final String SHOW_MESSAGE = "ShowMessage";

    public static final String TYPE = "Type";

    public static final String TABLE_NAME = "TableName";

    public static final String COLUMN_NAME = "ColumnName";

    public static final String CONTAIN_CONSTRAINTS = "ContainConstraints";

    public static final String KEYS = "Keys";

    public static final String GENERATING_ASSEMBLY = "GeneratingAssembly";

    public static final String IS_USE_NAMESPACE_CONFIG = "IsUseNamespaceConfig";

    // Udt相关
    public static final String IS_UDT = "IsUdt";

    public static final String UDT_PKG_NAME = "UdtPkgName";

    public static final String UDT_ID = "UdtID";

    public static final String UDT_NAME = "UdtName";

    public static final String UNIFIED_DATA_TYPE = "UnifiedDataType";

    public static final String MAPPING_RELATION = "MappingRelation";

    public static final String MAPPING_INFO = "MappingInfo";

    public static final String CHILD_ELEMENTS = "ChildElements";

    public static final String KEY_INFO = "KeyInfo";

    public static final String VALUE_INFO = "ValueInfo";

    public static final String INDEX = "Index";

    public static final String IS_DEFAULT_ENUM = "IsDefaultEnum";

    // 变量
    public static final String VARIABLE_SOURCE_TYPE = "VariableSourceType";
    // 版本字段
    public static final String VERSION_CONTROL_INFO = "VersionControlInfo";

    public static final String VERSION_CONTROL_ELEMENT_ID = "VersionControlElementId";

}