/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.shell;

import java.io.File;

public class Utils {
    public static final String SPRING_SHELL_FLAG_NAME = "spring-shell";
    public static final String SPRING_SHELL_FLAG_TRUE = "1";

    public static void delDir(String filePath) {
        File file = new File(filePath);
        if (file.exists())
            delDir(file);
    }

    public static void delDir(File file) {
        if (file.isDirectory()) {
            File zFiles[] = file.listFiles();
            for (File file2 : zFiles) {
                delDir(file2);
            }
            file.delete();
        } else {
            file.delete();
        }
    }

    public static String createTempDir() {
        File file = new File("packages");
        if (file.exists())
            file = new File("packages" + Runtime.getRuntime());
        file.mkdir();
        return file.getPath();
    }
}
