/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.common.configuration;

import com.inspur.edp.lcm.metadata.api.ConfigData.MetadataConfiguration;
import com.inspur.edp.lcm.metadata.spi.MetadataContentSerializer;

/**
 * @Classname MetadataSerializerHelper
 * @Description 元数据序列化器帮助
 * @Date 2019/7/24 9:31
 * @Created by zhongchq
 * @Version 1.0
 */
public class MetadataSerializerHelper extends MetadataConfigurationLoader {

    private static MetadataSerializerHelper singleton = null;

    public MetadataSerializerHelper() {
    }

    public static MetadataSerializerHelper getInstance() {
        if (singleton == null) {
            singleton = new MetadataSerializerHelper();
        }
        return singleton;
    }

    /**
     * @param typeName
     * @return com.inspur.edp.lcm.metadata.spi.MetadataContentSerializer
     * @throws
     * @author zhongchq
     * @description 返回各元数据序列化器
     * @date 9:34 2019/7/24
     **/
    public MetadataContentSerializer getManager(String typeName) {
        MetadataContentSerializer manager = null;
        MetadataConfiguration data = getMetadataConfigurationData(typeName);
        if (data != null) {
            Class<?> cls = null;
            try {
                if (data.getSerializer() != null) {
                    cls = Class.forName(data.getSerializer().getName());
                    manager = (MetadataContentSerializer) cls.newInstance();
                }
            } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
        return manager;
    }
}
