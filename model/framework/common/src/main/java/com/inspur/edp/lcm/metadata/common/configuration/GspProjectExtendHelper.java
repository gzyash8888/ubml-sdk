/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.common.configuration;

import com.inspur.edp.lcm.metadata.api.service.FileService;
import com.inspur.edp.lcm.metadata.common.FileServiceImp;
import io.iec.edp.caf.common.environment.EnvironmentUtil;

/**
 * Classname GspProjectExtendHelper Description TODO Date 2019/10/28 11:20
 *
 * @author zhongchq
 * @version 1.0
 */
public class GspProjectExtendHelper {
    private final String gspProjectExtend = "config/platform/common/lcm_gspprojectextend.json";
    private final String archetypePom = "platform/dev/main/libs/resources/archetypefront/src/main/resources/archetype-resources/pom.xml";
    private static GspProjectExtendHelper singleton = null;

    FileService fileService = new FileServiceImp();

    public GspProjectExtendHelper() {

    }

    public static GspProjectExtendHelper getInstance() {
        if (singleton == null) {
            singleton = new GspProjectExtendHelper();
        }
        return singleton;
    }

    public String getMavenProjectName() {
        MetadataServiceHelper metadataServiceHelper = new MetadataServiceHelper();
        try {
            final String combinePath = fileService.getCombinePath(EnvironmentUtil.getServerRTPath(), gspProjectExtend);
            return metadataServiceHelper.getMavenProjectName(combinePath);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public String getEdpParentVersion() {
        MetadataServiceHelper metadataServiceHelper = new MetadataServiceHelper();
        try {
            final String combinePath = fileService.getCombinePath(EnvironmentUtil.getServerRTPath(), archetypePom);
            return metadataServiceHelper.getEdpParentVersion(combinePath);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }
}
