/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.servermanager.monitor;

import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.HiddenFileFilter;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.monitor.FileAlterationListenerAdaptor;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;

public class MdpkgAlterationMonitor {
    private String path;        // 文件夹目录

    private String fileSuffix;    // 需要监听的文件名后缀

    private long interval;        // 监听间隔

    private static final long DEFAULT_INTERVAL = 10 * 1000; // 默认监听间隔10s

    private boolean running = false;

    private FileAlterationListenerAdaptor listener;    // 事件处理类对象

    private FileAlterationMonitor monitor;

    public MdpkgAlterationMonitor() {
        this.interval = DEFAULT_INTERVAL;
    }

    public MdpkgAlterationMonitor(String path, String fileSuffix, FileAlterationListenerAdaptor listenerAdaptor) {
        this.path = path;
        this.fileSuffix = fileSuffix;
        this.interval = DEFAULT_INTERVAL;
        this.listener = listenerAdaptor;
    }

    public MdpkgAlterationMonitor(String path, String fileSuffix, long interval,
        FileAlterationListenerAdaptor listenerAdaptor) {
        this.path = path;
        this.fileSuffix = fileSuffix;
        this.interval = interval;
        this.listener = listenerAdaptor;
    }

    /***
     * 开启监听
     */
    public void start() {
        if (path == null) {
            throw new IllegalStateException("Listen path must not be null");
        }
        if (listener == null) {
            throw new IllegalStateException("Listener must not be null");
        }

        IOFileFilter directories = FileFilterUtils.and(
            FileFilterUtils.directoryFileFilter(),
            HiddenFileFilter.VISIBLE);
        IOFileFilter files = FileFilterUtils.and(
            FileFilterUtils.fileFileFilter(),
            FileFilterUtils.suffixFileFilter(fileSuffix));
        IOFileFilter filter = FileFilterUtils.or(directories, files);
        // 设定观察者，监听文件
        FileAlterationObserver observer = new FileAlterationObserver(path, filter);
        // 给观察者添加监听事件
        observer.addListener(listener);

        // 开启一个监视器，监听频率是5s一次
        // FileAlterationMonitor本身实现了 Runnable，是单独的一个线程，按照设定的时间间隔运行，默认间隔是 10s
        monitor = new FileAlterationMonitor(interval);

        monitor.addObserver(observer);

        try {
            monitor.start();
            running = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public void checkAndNotify() {
//        if (running) {
//            Iterator i$ = this.monitor.getObservers().iterator();
//            while(i$.hasNext()) {
//                FileAlterationObserver observer = (FileAlterationObserver)i$.next();
//                observer.checkAndNotify();
//            }
//        }
//    }
}
