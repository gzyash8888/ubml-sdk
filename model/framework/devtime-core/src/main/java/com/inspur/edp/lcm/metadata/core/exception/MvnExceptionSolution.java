/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core.exception;

public class MvnExceptionSolution {
    public static final String NOT_FIND_SYMBOL_SOLUTION = "请确认是否已点击保存并同步。";

    public static final String NOT_EXIST_SOLUTION = "请确认是否已点击保存并同步。";

    public static final String NOT_ABSTRACT_SOLUTION = "请确认edp-parent的版本是否为最新。";

    public static final String DEFINED_NAME_SOLUTION = "请确认是否存在字段名相同的关联。";

    public static final String NOT_BE_RESOLVED_SOLUTION = "请确认仓库中是否存在以下包。";

    public static final String CLASS_NOT_AVAILABLE_SOLUTION = "请将以下符号保持一致。";

    public static final String NOT_ACCESSIBLE_SOLUTION = "请确认包中是否存在以下文件。";

    public static final String ERROR_IN_OPENING_ZIP_FILE_SOLUTION = "请删除本地仓库中的以下包。";

    public static final String LOG_REMINDER = "可在输出框查看详细错误日志。";
}
