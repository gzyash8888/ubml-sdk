/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package webapi;

import com.inspur.edp.lcm.metadata.api.entity.MetadataPackageHeader;
import com.inspur.edp.lcm.metadata.api.entity.MetadataPackageVersion;
import com.inspur.edp.lcm.metadata.api.entity.MetadataProject;
import com.inspur.edp.lcm.metadata.api.entity.MetadataProjectDto;
import com.inspur.edp.lcm.metadata.api.entity.bo.BOInfo;
import com.inspur.edp.lcm.metadata.api.service.MetadataProjectService;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import com.inspur.edp.lcm.metadata.devcommon.ManagerUtils;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MetadataProjectServiceWebApi {
    private MetadataProjectService metadataProjectService;

    private MetadataProjectService getMetadataProjectService() {
        if (metadataProjectService == null) {
            metadataProjectService = SpringBeanUtils.getBean(MetadataProjectService.class);
        }
        return metadataProjectService;
    }

    private MetadataService metadataService;

    private MetadataService getMetadataService() {
        if (metadataService == null) {
            metadataService = SpringBeanUtils.getBean(MetadataService.class);
        }
        return metadataService;
    }

    /**
     * 获取元数据工程信息
     *
     * @param path 元数据工程所在的路径或者下级路径
     * @return
     */
    @GET
    public MetadataProject getMetadataProjInfo(@QueryParam(value = "path") String path) {
        MetadataProject metadataProjInfo = getMetadataProjectService().getMetadataProjInfo(path);
        metadataProjInfo.setProjectPath(ManagerUtils.getRalativePath(metadataProjInfo.getProjectPath()));
        return metadataProjInfo;
    }

    @GET
    @Path("bo")
    public List<BOInfo> getBOInfo(@QueryParam("boids") String boids) {
        String[] boidArray = boids.split(",");
        return getMetadataProjectService().getBOInfo(boidArray);
    }

    /**
     * 根据工程或者下级路径，获取工程路径
     *
     * @param path 工程或者下级路径
     * @return
     */
    @GET
    @Path("/path")
    public String getProjPath(@QueryParam(value = "path") String path) {
        String projPath = getMetadataProjectService().getProjPath(path);
        String ralativePath = ManagerUtils.getRalativePath(projPath);
        return ralativePath;
    }

    /**
     * 元数据工程更新引用信息
     *
     * @param metadataProjectDto 需要传递ProjectPath、DepPackageName、DepPackageVersion、DepPackageLocation等信息
     */
    @PUT
    public void updateReference(MetadataProjectDto metadataProjectDto) {
        MetadataPackageHeader metadataPackageHeader = new MetadataPackageHeader();
        metadataPackageHeader.setName(metadataProjectDto.getDepPackageName());
        metadataPackageHeader.setVersion(new MetadataPackageVersion(metadataProjectDto.getDepPackageVersion()));
        metadataPackageHeader.setLocation(metadataProjectDto.getDepPackageLocation());
        getMetadataProjectService().updateRefs(metadataProjectDto.getProjectPath(), metadataPackageHeader);
    }

    /**
     * 元数据工程文件是否存在，用在前端判断是否可以执行打包操作
     *
     * @param path 路径信息，相对路径
     * @return
     */
    @GET
    @Path("/validationrecursively")
    public boolean isExistProjFileRecursively(@QueryParam(value = "path") String path) {
        return getMetadataProjectService().isExistProjFileRecursively(path);
    }

    /**
     * 获取bo路径
     *
     * @param path 工程路径
     * @return
     */
    @GET
    @Path("/bopath")
    public String getBoPath(@QueryParam(value = "path") String path) {
        return getMetadataProjectService().getBoPath(path);
    }

    /**
     * 获取是否解析型
     *
     * @param path 工程路径
     * @return
     */
    @GET
    @Path("/isInterpretation")
    public boolean isInterpretation(@QueryParam(value = "path") String path) {
        return getMetadataProjectService().isInterpretation(path);
    }

    /**
     * 当前目录是否存在，已废弃，使用getProjPath代替
     *
     * @param path
     * @return
     */
    @Deprecated
    @GET
    @Path("/validation")
    public boolean isExistProjectFile(@QueryParam(value = "path") String path) {
        return getMetadataProjectService().isExistProjFile(path);
    }

    /**
     * 获取技术栈，已废弃，仅保留java技术栈
     *
     * @param path 工程路径
     * @return
     */
    @Deprecated
    @GET
    @Path("/codelanguage")
    public String getCodeLanguage(@QueryParam(value = "path") String path) {
        return "java";
    }

    /**
     * 是否使用maven，都使用maven，所以废弃
     *
     * @param path 工程路径
     * @return
     */
    @Deprecated
    @GET
    @Path("/isMaven")
    public boolean getMavenStatus(@QueryParam(value = "path") String path) {
        return true;
    }

    /**
     * 更新元数据缓存路径
     *
     * @param path 工程路径
     */
    @Deprecated
    @PUT
    @Path("/metadataPath")
    public void setMetadataPath(@QueryParam(value = "path") String path) {
        getMetadataService().setMetadataUri(path);
    }
}
