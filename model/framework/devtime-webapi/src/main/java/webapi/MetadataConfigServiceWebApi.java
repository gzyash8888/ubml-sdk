/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package webapi;

import com.inspur.edp.lcm.metadata.api.entity.MetadataType;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MetadataConfigServiceWebApi {
    private MetadataService metadataService;

    private MetadataService getMetadataService() {
        if (metadataService == null) {
            metadataService = SpringBeanUtils.getBean(MetadataService.class);
        }
        return metadataService;
    }

    /**
     * 获取各元数据配置的元数据类型信息及后缀信息
     *
     * @return
     */
    @GET
    public List<MetadataType> getMetadataTypeList() {
        return getMetadataService().getMetadataTypeList();
    }

    /**
     * 更新元数据缓存路径
     *
     * @param path 工程路径
     */
    @PUT
    public void setMetadataUri(@QueryParam(value = "path") String path) {
        getMetadataService().setMetadataUri(path);
    }
}
