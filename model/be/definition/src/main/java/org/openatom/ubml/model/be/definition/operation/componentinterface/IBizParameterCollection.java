/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.operation.componentinterface;

/**
 * The Collection Of The Parameter
 *
 * @ClassName: IBizParameterCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public interface IBizParameterCollection<T extends IBizParameter> extends Iterable<T> {
    IBizParameter getItem(int index);

    /**
     * @param item The Parameter To Add
     * @return
     */
    boolean add(IBizParameter item);

    /**
     * @param index The Position Of The Parameter To Insert
     * @param item  The Parameter To Insert
     */
    void insert(int index, IBizParameter item);

    /**
     * @param index The Postion Of The Parameter To Remove
     */
    void removeAt(int index);

    /**
     * @param item The Parameter To Remove
     * @return
     */
    boolean remove(IBizParameter item);

    /**
     * @param item The Parameter To Judge Wether Contains
     * @return
     */
    boolean contains(IBizParameter item);

    /**
     * Clear All  Parameters
     */
    void clear();

    /**
     * @param item The Parameter To Find Index
     * @return
     */
    int indexOf(IBizParameter item);

    /**
     * @return The Parameters Count Of The Colledtion
     */
    int getCount();
}