/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.openatom.ubml.model.be.definition.common.BizEntityJsonConst;
import org.openatom.ubml.model.be.definition.operation.componentbase.BizParameter;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.common.definition.commonmodel.json.CommonModelNames;

/**
 * The  Josn Serializer Of Biz Parameter
 *
 * @ClassName: BizParameterSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class BizParameterSerializer<T extends BizParameter> extends JsonSerializer<T> {
    @Override
    public void serialize(BizParameter value, JsonGenerator gen, SerializerProvider serializers) {

        WriteOperationInfo(gen, value);
    }

    private void WriteOperationInfo(JsonGenerator writer, BizParameter para) {
        //{
        SerializerUtils.writeStartObject(writer);
        SerializerUtils.writePropertyValue(writer, CommonModelNames.ID, para.getID());
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.PARAM_CODE, para.getParamCode());
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.PARAM_NAME, para.getParamName());
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.PARAMETER_TYPE, para.getParameterType().getValue());
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.COLLECTION_PARAMETER_TYPE, para.getCollectionParameterType().getValue());
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.ASSEMBLY, para.getAssembly());
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.CLASS_NAME, para.getNetClassName());
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.JAVA_CLASS_NAME, para.getClassName());
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.MODE, para.getMode().getValue());
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.PARAM_DESCRIPTION, para.getParamDescription());
        //}
        SerializerUtils.writeEndObject(writer);
    }
}
