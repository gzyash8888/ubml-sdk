/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.deser.std.StringDeserializer;
import java.util.EnumSet;
import java.util.HashMap;
import org.openatom.ubml.model.be.definition.beenum.BETriggerTimePointType;
import org.openatom.ubml.model.be.definition.beenum.BEValidationType;
import org.openatom.ubml.model.be.definition.beenum.RequestNodeTriggerType;
import org.openatom.ubml.model.be.definition.common.BizEntityJsonConst;
import org.openatom.ubml.model.be.definition.operation.BizOperation;
import org.openatom.ubml.model.be.definition.operation.Validation;
import org.openatom.ubml.model.common.definition.cef.collection.ValElementCollection;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;

/**
 * The  Josn Deserializer Of Validation
 *
 * @ClassName: BizValidationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class BizValidationDeserializer extends BizOperationDeserializer<Validation> {
    @Override
    protected Validation createBizOp() {
        return new Validation();
    }

    @Override
    protected boolean readExtendOpProperty(BizOperation op, String propName, JsonParser jsonParser) {
        Validation validation = (Validation)op;
        boolean hasProperty = true;
        switch (propName) {
            case BizEntityJsonConst.VALIDATION_TYPE:
                validation.setValidationType(SerializerUtils.readPropertyValue_Enum(jsonParser, BEValidationType.class, BEValidationType.values()));
                break;
            case BizEntityJsonConst.TRIGGER_TIME_POINT_TYPE:
                validation.setTriggerTimePointType(readBETriggerTimePointType(jsonParser));
                break;
            case BizEntityJsonConst.ORDER:
                SerializerUtils.readPropertyValue_Integer(jsonParser);
                break;
            case BizEntityJsonConst.PRECEDING_IDS:
                SerializerUtils.readStringArray(jsonParser);
                break;
            case BizEntityJsonConst.SUCCEEDING_IDS:
                SerializerUtils.readStringArray(jsonParser);
                break;
            case BizEntityJsonConst.REQUEST_NODE_TRIGGER_TYPE:
                validation.setRequestNodeTriggerType(readRequestNodeTriggerType(jsonParser));
                break;
            case BizEntityJsonConst.REQUEST_ELEMENTS:
                validation.setRequestElements(readElementArray(jsonParser));
                break;
            case BizEntityJsonConst.REQUEST_CHILD_ELEMENTS:
                validation.setRequestChildElements(readRequestChildElements(jsonParser));
                break;
            case BizEntityJsonConst.VALIDATION_TRIGGER_POINTS:
                validation.setValidationTriggerPoints(readValidationTriggerPoints(jsonParser));
                break;
            default:
                hasProperty = false;
        }
        return hasProperty;
    }

    private HashMap<String, ValElementCollection> readRequestChildElements(JsonParser jsonParser) {
        RequestChildElementsDeserializer deserializer = new ValRequestChildElementsDeserializer();
        return deserializer.deserialize(jsonParser, null);
    }

    private ValElementCollection readElementArray(JsonParser jsonParser) {
        ValElementCollection collection = new ValElementCollection();
        SerializerUtils.readArray(jsonParser, new StringDeserializer(), collection, true);
        return collection;
    }

    private HashMap<BETriggerTimePointType, EnumSet<RequestNodeTriggerType>> readValidationTriggerPoints(JsonParser jsonParser) {
        ValidatoinTriggerPointsDeserializer deserializer = new ValidatoinTriggerPointsDeserializer();
        HashMap<BETriggerTimePointType, EnumSet<RequestNodeTriggerType>> dic = deserializer.deserialize(jsonParser, null);
        return dic;
    }
}
