/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.vo.definition.action.internalexternalaction;

import org.openatom.ubml.model.vo.definition.action.MappedCdpAction;
/**
 * The Definition Of Delete Action
 *
 * @ClassName: DeleteAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class DeleteAction extends MappedCdpAction implements IInternalExtendAction {
    public static final String ID = "9a17e935-7366-489d-b110-0ae103e5648e";
    public static final String CODE = "Delete";
    public static final String NAME = "内置删除操作";

    public DeleteAction() {
        setID(ID);
        setCode(CODE);
        setName(NAME);
    }
}
